import 'package:flutter/material.dart';

import 'bloc.dart';

class BlocProvider<T extends Bloc> extends StatefulWidget {
  const BlocProvider({
    super.key,
    required this.bloc,
    required this.child,
  });

  // Le bloc
  final T bloc;
  // Le child va s'accuper du bloc
  final Widget child;

  // Valeur du Type
  static Type providerType<T>() => T;

  // Configurer le Bloc
  static T of<T extends Bloc>(BuildContext context) {
    // TODO : Ici, obligé de mettre un ? car le retour est nullable.
    final BlocProvider<T>? provider =
        context.findAncestorWidgetOfExactType<BlocProvider<T>>();
    return provider!.bloc;
  }

  @override
  State<BlocProvider> createState() => _BlocProviderState();
}

class _BlocProviderState extends State<BlocProvider> {
  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
