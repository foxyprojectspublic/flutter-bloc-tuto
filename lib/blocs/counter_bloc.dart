import 'dart:async';

import 'bloc.dart';

class CounterBloc extends Bloc {
  // Donnée
  int count = 0;

  // Construction du bloc
  CounterBloc() {
    sink.add(count);
  }

  // StreamController
  final streamController = StreamController<int>();

  // Entrée
  Sink<int> get sink => streamController.sink;

  // Sortie
  Stream<int> get stream => streamController.stream;

  // Logique métier
  incrementCounter() {
    count += 1;
    sink.add(count);
  }

  decrementCounter() {
    count -= 1;
    sink.add(count);
  }

  // Fermeture du stream
  @override
  dispose() => streamController.close();
}
