import 'package:blocs_tuto/blocs/bloc_provider.dart';
import 'package:blocs_tuto/blocs/counter_bloc.dart';
import 'package:blocs_tuto/pages/counter.dart';
import 'package:flutter/material.dart';

class AppPagesHome extends StatelessWidget {
  const AppPagesHome({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Choisir le bloc"),
            TextButton(
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => BlocProvider<CounterBloc>(
                    bloc: CounterBloc(),
                    child: const AppPagesCounter(),
                  ),
                ),
              ),
              child: const Text("Counter with BlocProvider"),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const AppPagesCounter(),
                ),
              ),
              child: const Text("Counter without BlocProvider"),
            ),
          ],
        ),
      ),
    );
  }
}
