import 'package:flutter/material.dart';

class AppPagesText extends StatelessWidget {
  const AppPagesText({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: const Center(
        child: Text("compteur text"),
      ),
    );
  }
}
